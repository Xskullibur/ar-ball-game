using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    // GameObjects
    public GameObject ball;
    public Button resetButton;

    // Variables
    float startTime, endTime, swipeDistance, swipeTime;
    private Vector2 startPos;
    private Vector2 endPos;

    public float MinSwipDist = 0;
    private float BallVelocity = 0;
    private float BallSpeed = 0;
    public float MaxBallSpeed = 350;
    private Vector3 angle;

    private bool thrown, holding;
    private Vector3 newPosition;
    Rigidbody rb;

    // Initialize any variables or game state before the game starts
    private void Awake()
    {
        resetButton.onClick.AddListener(ResetBall);
    }

    // Start is called before the first frame update
    void Start()
    {
        SetupBall();
    }

    /// <summary>
    /// Method <c>SetupBall</c> prepares all the variable to be used and calls <c>ResetBall</c>
    /// </summary>
    private void SetupBall()
    {
        // Get Rigidbody of the ball
        rb = ball.GetComponent<Rigidbody>();
        ResetBall();
    }

    /// <summary>
    /// Method <c>ResetBall</c> resets all the varibles tied to the ball to the default values.
    /// </summary>
    private void ResetBall()
    {
        angle = Vector3.zero;
        endPos = Vector2.zero;
        startPos = Vector2.zero;
        BallSpeed = 0;
        startTime = 0;
        endTime = 0;
        swipeDistance = 0;
        swipeTime = 0;
        thrown = holding = false;
        rb.velocity = Vector3.zero;
        rb.useGravity = false;
        ball.transform.position = new Vector3(0, 0.5f, 4);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            
            // When user interacts with the game
            if (touch.phase == TouchPhase.Began)
            {
                // Get the position of screen reletive to the Game World
                Ray ray = Camera.current.ScreenPointToRay(touch.position);
                RaycastHit _hit;

                // Store variables to calculate velocity and angle
                if (Physics.Raycast(ray, out _hit, 100f))
                {
                    if (_hit.transform == ball.transform)
                    {
                        startTime = Time.time;
                        startPos = Input.mousePosition;
                        holding = true;
                    }
                }
            }

            // When user repositions the ball
            if (touch.phase == TouchPhase.Moved)
            {
                // Handle repositioning of ball when touch move detected
                if (holding)
                {
                    PickupBall(touch);
                }
            }

            // When user releases the ball (stop touching the screen)
            if (touch.phase == TouchPhase.Ended)
            {
                // Get end variables
                endTime = Time.time;
                endPos = Input.mousePosition;
                swipeDistance = (endPos - startPos).magnitude;
                swipeTime = endTime - startTime;

                // Check if its within threshold
                if (swipeTime < 0.5f && swipeDistance > 30f)
                {
                    // Calculate velocity and angle
                    CalculateVelocity();
                    CalculateAngle();

                    // Enable RigidBody variables to throw the ball
                    rb.AddForce(new Vector3((angle.x * BallSpeed), (angle.y * BallSpeed / 3), (angle.z * BallSpeed) * 2));
                    rb.useGravity = true;
                    holding = false;
                    thrown = true;
                    //Invoke("ResetBall", 4f);
                }
                else
                {
                    // Reset the ball to original position
                    ResetBall();
                }
            }
        }
    }

    /// <summary>
    /// Method <c>PickupBall</c> clips the Ball GameObject to the Input and follows its position
    /// </summary>
    private void PickupBall(Touch touch)
    {
        Vector3 touchPosition = touch.position;
        touchPosition.z = Camera.current.nearClipPlane * 40f;
        newPosition = Camera.current.ScreenToWorldPoint(touchPosition);
        ball.transform.localPosition = Vector3.Lerp(ball.transform.localPosition, newPosition, 80f * Time.deltaTime);
    }

    // Calculate angle of movement
    private void CalculateAngle()
    {
        angle = Camera.current.ScreenToWorldPoint(new Vector3(endPos.x, endPos.y + 50f, (Camera.current.nearClipPlane + 5)));
    }

    void CalculateVelocity()
    {
        if (swipeTime > 0)
            BallVelocity = swipeDistance / (swipeDistance - swipeTime);

        BallSpeed = BallVelocity * 50;

        if (BallSpeed <= MaxBallSpeed)
        {
            BallSpeed = MaxBallSpeed;
        }

        swipeTime = 0;
    }

}
